// console.log("Hello, B204!");


//name - parameter
function printName(name) {

	console.log("My name is " + name);

};

//data passed into a function invocation can be received by the function
//this is what we call an "argument"
printName("Aron");

function checkDivisibilityBy8(num) {

	let remainder = num % 8
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);

}

checkDivisibilityBy8(456);

//variables can also be passed as an argument
let sampleVariable = 64;
checkDivisibilityBy8(sampleVariable);

/*
	Mini-activity:
		1. create a function which is able to receive data as an argument
			-this function should be able to receive the name of your favorite superhero
			-display the name of your favorite superhero in the console
*/

function favoriteSuperhero(superheroName) {

	console.log("My favorite superhero is " + superheroName);

}

favoriteSuperhero("Spiderman");

//arrays can be used as an argument

//using multiple parameters
//multiple argument will correspond to the number of "parameters" declared in a function in succeeding order
function createFullName(firstName, middleName, lastName) {

	console.log(firstName + " " + middleName + " " + lastName);

}

createFullName("Edwin Aron", "Giron", "Rovero");

//using variables as arguments
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

/*
	Mini-activity:
		create a function which will be able to receive 5 arguments
			-receive 5 of your favorite songs
			-display/print the passed 5 favorite songs in the console when the function is invoked
*/

function favoriteSongs(song1, song2, song3, song4, song5) {

	console.log("My favorite songs are: " + song1 + song2 + song3 + song4 + song5);

}

favoriteSongs("Wherever I May Roam, ", "Lover, ", "Hanggang Kailan, ", "Foe Tha Love of Money, ", "and Nocturne op. 55 no. 1");

//return statement
//the "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function

function returnFullName(firstName, middleName, lastName) {
	
	console.log("This message will be printed");
	return firstName + " " + middleName + " " + lastName;
	console.log("This message will not be printed");

}

let completeName = returnFullName("Jane", "Shatur", "Regina");
console.log(completeName);
//console.log(returnFullName("Jane", "Shatur", "Regina"));

function returnAddress(city, country) {

	let fullAddress = city + ", " + country;
	return fullAddress

}

let myAddress = returnAddress("Manila", "Philippines");
console.log(myAddress);

function printPlayerInfo(username, level, job) {

	console.log("Username: " + username);
	console.log("Level: " + level);
	console.log("Job: " + job);

}

let user1 = printPlayerInfo("ikawLangSapatNa", 99, "Mage");
console.log(user1);